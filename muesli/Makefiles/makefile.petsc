# ****************************************************************************
# *
# *                                 M U E S L I   v 1.8
# *
# *
# *     Copyright 2020 IMDEA Materials Institute, Getafe, Madrid, Spain
# *     Contact: muesli.materials@imdea.org
# *     Author: Ignacio Romero (ignacio.romero@imdea.org)
# *
# *     This file is part of MUESLI.
# *
# *     MUESLI is free software: you can redistribute it and/or modify
# *     it under the terms of the GNU General Public License as published by
# *     the Free Software Foundation, either version 3 of the License, or
# *     (at your option) any later version.
# *
# *     MUESLI is distributed in the hope that it will be useful,
# *     but WITHOUT ANY WARRANTY; without even the implied warranty of
# *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# *     GNU General Public License for more details.
# *
# *     You should have received a copy of the GNU General Public License
# *     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
# *
# ****************************************************************************

INSTALL_BASE := ../lib

export SOURCEDIR   = $(CURDIR)
export MUESLI_PATH = $(CURDIR)
export MUESLI_SRC  = $(SOURCEDIR)
export LIBPATH     = $(MUESLI_PATH)/../lib

# to use EIGEN uncomment the next line
#export WITHEIGEN = true

ifdef WITHEIGEN
	export CPPFLAGS = -DWITHEIGEN -DNDEBUG
	export EIGEN_PATH = /usr/local/include
endif


# detect operating system, set ARCH variable
OS:=$(shell uname)
ifeq ($(OS), Darwin)
	export ARCH	   := darwin_ia64
else ifeq ($(OS), Linux)
	export ARCH  := linux_ia64
endif

export MUESLI_LIB  = libmuesli_mpi_$(ARCH).a


# maintenance functions
export REMOVE	 = @rm
export TIDY		 = @rm -f *.bak *~ .DS_Store *.o *.log
export CLEAN	 = @rm -f *.bak *.o *~ .DS_Store

INCLUDEPATH		 = -I$(SOURCEDIR)/..
AR			 = /usr/bin/ar
ARFLAGS			 = cr
export CPP		 = g++

ifeq ($(ARCH),darwin_ia64)

     PETSC_ARCH = macos_ia64.withmpi
     petsc.pc := $(PETSC_DIR)/$(PETSC_ARCH)/lib/pkgconfig/PETSc.pc
     PACKAGES := $(petsc.pc)

     INCLUDEPATH += -I$(PETSC_DIR)/include -I$(shell pkg-config --variable=includedir $(PACKAGES))

	 CC := $(shell pkg-config --variable=ccompiler $(PACKAGES))
	 CXX := $(shell pkg-config --variable=cxxcompiler $(PACKAGES))
	 FC := $(shell pkg-config --variable=fcompiler $(PACKAGES))

	 CFLAGS_OTHER := $(shell pkg-config --cflags-only-other $(PACKAGES))
	 CFLAGS := $(shell pkg-config --variable=cflags_extra $(PACKAGES)) $(CFLAGS_OTHER)

	 CXXFLAGS := $(shell pkg-config --variable=cxxflags_extra $(PACKAGES)) $(CFLAGS_OTHER)
	 FFLAGS := $(shell pkg-config --variable=fflags_extra $(PACKAGES))
	 CPPFLAGS := $(shell pkg-config --cflags-only-I $(PACKAGES))
	 LDFLAGS := $(shell pkg-config --libs-only-L --libs-only-other $(PACKAGES))
	 LDFLAGS += $(patsubst -L%, $(shell pkg-config --variable=ldflag_rpath $(PACKAGES))%, $(shell pkg-config --libs-only-L $(PACKAGES)))
	 LDLIBS := $(shell pkg-config --libs-only-l $(PACKAGES)) -lm
	 CUDAC := $(shell pkg-config --variable=cudacompiler $(PACKAGES))
	 CUDAC_FLAGS := $(shell pkg-config --variable=cudaflags_extra $(PACKAGES))
	 CUDA_LIB := $(shell pkg-config --variable=cudalib $(PACKAGES))
	 CUDA_INCLUDE := $(shell pkg-config --variable=cudainclude $(PACKAGES))

     CPPFLAGS := $(CXXFLAGS)
     export CPPFLAGS
     ALL_LIBS = $(LDLIBS) -lextra_mpi_mac

     #change petsc variables to iris variables
     export CC
     CPP = $(CXX)
     export CPP
     export CPPFLAGS  = $(CXXFLAGS) $(INCLUDEPATH)
     export CFLAGS    = -Os $(INCLUDEPATH)

     LINKERFLAGS += -L$(PETSC_DIR)/$(PETSC_ARCH)/lib -L/usr/X11R6/lib
	 RANLIB			 = ranlib $(RLIBFLAGS)


else ifeq ($(ARCH),linux_ia64)
	export CPPFLAGS += -O3 -std=c++11 -fpic -Wall -arch=native $(INCLUDEPATH)
	LINKERFLAGS		+= -L$(LIBPATH)
	RANLIB			 = ranlib $(RLIBFLAGS)
	ifndef WITHEIGEN
		LINKERFLAGS += -llapack -lblas
	else
		INCLUDEPATH += -I$(EIGEN_PATH)
	endif
endif

SUBDIRS = \
	$(SOURCEDIR)/Damage	 \
	$(SOURCEDIR)/Failure  \
	$(SOURCEDIR)/Finitestrain  \
	$(SOURCEDIR)/Fluid	\
	$(SOURCEDIR)/Fcoupled \
	$(SOURCEDIR)/Interface	\
	$(SOURCEDIR)/Math  \
	$(SOURCEDIR)/Scoupled  \
	$(SOURCEDIR)/Smallstrain  \
	$(SOURCEDIR)/Thermal \
	$(SOURCEDIR)/Utils


MUESLI_FILES = \
	material.o	 \
	tensor.o \
	Damage/damage.o \
	Damage/jcdamage.o \
	Failure/brownmiller.o \
	Failure/jcfailure.o \
	Finitestrain/arrheniustype.o   \
	Finitestrain/arrudaboyce.o	 \
	Finitestrain/finitestrain.o	  \
	Finitestrain/fisotropic.o \
	Finitestrain/fplastic.o \
	Finitestrain/fplastic.o \
	Finitestrain/johnsoncook.o \
	Finitestrain/mooney.o \
	Finitestrain/neohook.o \
	Finitestrain/reducedfinitestrain.o \
	Finitestrain/svk.o \
	Finitestrain/yeoh.o \
	Finitestrain/zerilliarmstrong.o \
	Fluid/fluid.o \
	Fluid/newtonian.o \
	Fcoupled/thermofinitestrain.o \
	Fcoupled/fthermomechmass.o \
	Fcoupled/thermojc.o \
	Fcoupled/thermoza.o \
	Fcoupled/fmechmass.o \
	Interface/interface_abaqus.o \
	Interface/interface_lsdyna.o \
	Scoupled/sthermomech.o\
	Scoupled/sphasemech.o\
	Smallstrain/smallstrain.o	\
	Smallstrain/elastic.o \
	Smallstrain/reducedsmallstrain.o \
	Smallstrain/sdamage.o \
	Smallstrain/splastic.o \
	Smallstrain/viscoelastic.o \
	Smallstrain/viscoplastic.o \
	Thermal/conductor.o \
	Utils/utils.o

ifndef WITHEIGEN
	MUESLI_FILES +=\
	Math/mtensor.o \
	Math/mmatrix.o \
	Math/mrealvector.o
endif

default: library

export TASKS_FILE = $(SOURCEDIR)/makefile.tasks
include $(TASKS_FILE)

library: material.o tensor.o
	@( \
	for f in $(SUBDIRS); \
	do \
		$(MAKE) -C $$f compile; \
	done );
	($(AR) $(ARFLAGS) $(LIBPATH)/$(MUESLI_LIB) $(MUESLI_FILES))
	$(RANLIB) $(LIBPATH)/$(MUESLI_LIB)


install:
	test -d $(INSTALL_BASE) || mkdir $(INSTALL_BASE)
	test -d $(INSTALL_BASE)/lib || mkdir $(INSTALL_BASE)/lib
	test -d $(INSTALL_BASE)/include || mkdir $(INSTALL_BASE)/include
	rm -rf $(INSTALL_BASE)/include/muesli
	cp -p $(LIBPATH)/$(MUESLI_LIB) $(INSTALL_BASE)/lib
	find . -name '*.h' -print | cpio -pdlv $(INSTALL_BASE)/include/muesli

#   find . -name '*.h' -exec install -m 0755 '{}' $(INSTALL_BASE)/include/muesli ';'


all: library install
	rm -f Test/test.o
	@( $(MAKE) -C Test all );
	g++ Test/test.o -lmuesli -o testmuesli
	./testmuesli

example: Test/example.o
	@( $(MAKE) -C Test compile);
	g++ -L$(LIBPATH) Test/example.o -lmuesli -o Test/examplemuesli

help:
	@echo "MUESLI, the Material UnivErSal LIbrary"
	@echo "Usage:"
	@echo "  make, to build the library and install it"
	@echo "  make clean, to remove all but critical files"
	@echo "  make library, to build libmuesli.a"
	@echo "  make test, to compile the test program"
	@echo "  make tidy, to remove all intermediate files"
	@echo "  make zip, to compress the MUESLI creating a zip file"

test: Test/test.o
	@( $(MAKE) -C Test compile );
	g++ -L$(LIBPATH) -framework Accelerate Test/test.o -lmuesli -o testmuesli
	@( rm -f Test/test.o )

# use this test if linking with a developing library, not installed yet
devtest: library Test/test.o
	@( $(MAKE) -C Test compile );
	g++ Test/test.o -L$(LIBPATH) $(LINKERFLAGS) -lmuesli -o testmuesli
	./testmuesli

runexample: library example
	@( cd Test; ./examplemuesli; gnuplot generate.gnuplot; mpost example.mp; mptopdf example.0 example.1)
	@( cd Test; rm -f mpx* *.mp *.mpx example.0)

zip: clean
	(cd ..; tar czvf muesli.tgz muesli)

maintenance:
	find . -name '*.h' -print0 | xargs -0 sed -E -i '' 's/subdirs/SUBDIRS/g'

headers:
	find . -name "*.h" -exec replace_header.sh {} \;
	find . -name "*.cpp" -exec replace_header.sh {} \;
	find . -name "makefile" -exec replace_header_makefile.sh {} \;
