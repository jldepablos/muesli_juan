/****************************************************************************
*
*                                 M U E S L I   v 1.8
*
*
*     Copyright 2020 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/



#include <stdio.h>
#include "smechmass.h"
#include "muesli/Smallstrain/smallstrainlib.h"

using namespace muesli;


sMechMassMaterial::sMechMassMaterial(const std::string& name,
                                     const materialProperties& cl)
:
material(name, cl),
theSSMaterial(0),
cref(1e-5),
diffusivity(0.0),
massExpansion(0.0),
muref(1.0),
R(8.31),
tref(273.0)
{
    if       (cl.find("subtype elastic") != cl.end())      theSSMaterial = new elasticIsotropicMaterial(name, cl);
    else if  (cl.find("subtype plastic") != cl.end())      theSSMaterial = new splasticMaterial(name, cl);
    else if  (cl.find("subtype viscoelastic") != cl.end()) theSSMaterial = new viscoelasticMaterial(name, cl);
    else if  (cl.find("subtype viscoplastic") != cl.end()) theSSMaterial = new viscoplasticMaterial(name, cl);

    muesli::assignValue(cl, "alpha", massExpansion);
    muesli::assignValue(cl, "cref", cref);
    muesli::assignValue(cl, "diffusivity", diffusivity);
    muesli::assignValue(cl, "muref", muref);
    muesli::assignValue(cl, "r", R);
    muesli::assignValue(cl, "tref", tref);

    gamma = 1.0/cref;
}




sMechMassMaterial::~sMechMassMaterial()
{
    if (theSSMaterial != 0) delete(theSSMaterial);
}




bool sMechMassMaterial::check() const
{
    return theSSMaterial->check();
}




sMechMassMP* sMechMassMaterial::createMaterialPoint() const
{
    return new sMechMassMP(*this);
}




double sMechMassMaterial::density() const
{
    return theSSMaterial->density();
}




double sMechMassMaterial::getProperty(const propertyName p) const
{
    double ret=0.0;

    switch (p)
    {
        case PR_DIFFUSIVITY:    ret = diffusivity; break;

        default:
            ret = theSSMaterial->getProperty(p);
    }
    return ret;
}




void sMechMassMaterial::print(std::ostream &of) const
{
    of  << "\n Small strain sMechMass material."
        << "\n for coupled stress-diffusion problems."
        << "\n Surrogate mechanical model: ";
        theSSMaterial->print(of);

    of  << "\n Reference temperature:                 " << tref
        << "\n Mass expansion coefficient:            " << massExpansion
        << "\n Mass diffusivity:                      " << diffusivity;
}




void sMechMassMaterial::setRandom()
{
    int mattype = discreteUniform(1, 1);
    std::string name = "surrogate small strain material";
    materialProperties mp;

    if (mattype == 0)
        theSSMaterial = new elasticIsotropicMaterial(name, mp);

    else if (mattype == 1)
        theSSMaterial = new splasticMaterial(name, mp);

    else if (mattype == 2)
        theSSMaterial = new viscoelasticMaterial(name, mp);

    else if (mattype == 3)
        theSSMaterial = new viscoplasticMaterial(name, mp);

    else if (mattype == 4)
        theSSMaterial = new sdamageMaterial(name, mp);

    theSSMaterial->setRandom();
    massExpansion  = randomUniform(1.0, 2.0);
    tref           = randomUniform(100.0, 400.0);
    diffusivity    = randomUniform(0.80, 1.0);
}




bool sMechMassMaterial::test(std::ostream& of)
{
    bool isok = true;
    setRandom();

    sMechMassMP* p = this->createMaterialPoint();

    isok = p->testImplementation(of);
    delete p;

    return isok;
}




double sMechMassMaterial::waveVelocity() const
{
    return theSSMaterial->waveVelocity();
}



sMechMassMP::sMechMassMP(const sMechMassMaterial &m)
:
mu_n(0.0), mu_c(0.0),
gradMu_n(0.0, 0.0, 0.0),
gradMu_c(0.0, 0.0, 0.0),
theSSMP(0),
theMechMassMaterial(m)
{
    theSSMP = m.theSSMaterial->createMaterialPoint();
}




sMechMassMP::~sMechMassMP()
{
    if (theSSMP != 0) delete (theSSMP);
}




double& sMechMassMP::chemicalPotential()
{
    return mu_c;
}




const double& sMechMassMP::chemicalPotential() const
{
    return mu_c;
}




void sMechMassMP::commitCurrentState()
{
    mu_n     = mu_c;
    gradMu_n = gradMu_c;
    theSSMP->commitCurrentState();
}




double sMechMassMP::concentration() const
{
    const double gamma = theMechMassMaterial.gamma;
    const double tref  = theMechMassMaterial.tref;
    const double R     = theMechMassMaterial.R;
    const double muref   = theMechMassMaterial.muref;

    return  1.0/gamma*exp((mu_c-muref)/(R*tref));
}




double sMechMassMP::contractWithDiffusivity(const ivector &v1, const ivector &v2) const
{
    return theMechMassMaterial.diffusivity * v1.dot(v2);
}




void sMechMassMP::contractWithTangent(const ivector &v1, const ivector &v2, itensor &T) const
{
    theSSMP->contractWithTangent(v1, v2, T);
}




void sMechMassMP::contractWithDeviatoricTangent(const ivector &v1, const ivector &v2, itensor &T) const
{
    theSSMP->contractWithDeviatoricTangent(v1, v2, T);
}




double sMechMassMP::deviatoricEnergy() const
{
    return theSSMP->deviatoricEnergy();
}




void sMechMassMP::deviatoricStress(istensor& s) const
{
    theSSMP->deviatoricStress(s);
}




void sMechMassMP::dissipationTangent(itensor4& D) const
{
    theSSMP -> dissipationTangent(D);
}




double sMechMassMP::effectiveStoredEnergy() const
{
/*
    double       dt_Psistar = energyDissipationInStep();
    double       fmec       = theSSMP->storedEnergy();
    double       fcou       = -3.0 * kappa * alpha * mu_c * vol;
    double       fthe       = -0.5 * cv/tref * mu_c * mu_c;
*/
    return 0.0;
}




double sMechMassMP::energyDissipationInStep() const
{
    double dissMech = theSSMP->energyDissipationInStep();
    double dissMass = 0.0;
    return dissMech + dissMass;
}




double sMechMassMP::freeEnergy() const
{
    const double alpha  = theMechMassMaterial.massExpansion;
    const double kappa  = theSSMP->volumetricStiffness();
    const double R      = theMechMassMaterial.R;
    const double tref   = theMechMassMaterial.tref;
    const double gamma  = theMechMassMaterial.gamma;
    const double muref  = theMechMassMaterial.muref;

    double vol   = theSSMP->getCurrentStrain().trace();
    double chic  = concentration();
    double fmec  = theSSMP->storedEnergy();
    double fcou  = -3.0 * kappa * alpha * (concentration()-theMechMassMaterial.cref) * vol;
    double fmass = muref*chic + R*tref*(chic*log(gamma*chic)-chic);

    return fmec + fcou + fmass;
}




istensor sMechMassMP::getConvergedPlasticStrain() const
{
    return theSSMP->getConvergedPlasticStrain();
}




materialState sMechMassMP::getConvergedState() const
{
    materialState mat = theSSMP->getConvergedState();

    mat.theDouble.push_back(mu_n);
    mat.theVector.push_back(gradMu_n);

    return mat;
}




istensor sMechMassMP::getCurrentPlasticStrain() const
{
    return theSSMP->getCurrentPlasticStrain();
}




materialState sMechMassMP::getCurrentState() const
{
    // first store the state of the ssmp
    materialState mat = theSSMP->getCurrentState();

    // then append the thermal data
    mat.theDouble.push_back(mu_c);
    mat.theVector.push_back(gradMu_c);

    return mat;
}




istensor& sMechMassMP::getCurrentStrain()
{
    return theSSMP->getCurrentState().theStensor[0];
}




double sMechMassMP::grandCanonicalPotential() const
{
    double psi = freeEnergy();

    return psi - mu_c*concentration();
}




ivector sMechMassMP::massflux() const
{
    const double kappa  = theSSMP->volumetricStiffness();
    const double R      = theMechMassMaterial.R;
    const double tref   = theMechMassMaterial.tref;

    return -kappa/(tref*R) * mu_c * gradMu_c;
}




double sMechMassMP::plasticSlip() const
{
    return theSSMP->plasticSlip();
}




double sMechMassMP::pressure() const
{
    return theSSMP->pressure();
}




void sMechMassMP::resetCurrentState()
{
    mu_c     = mu_n;
    gradMu_c = gradMu_n;
    theSSMP->resetCurrentState();
}




void sMechMassMP::setRandom()
{
    mu_c = randomUniform(0.06, 0.13) * theMechMassMaterial.tref;
    gradMu_c.setRandom();
    theSSMP->setRandom();
}




void sMechMassMP::stress(istensor& sigma) const
{
    const double alpha = theMechMassMaterial.massExpansion;
    const double kappa = theSSMP->volumetricStiffness();
    const double cref  = theMechMassMaterial.cref;

    theSSMP->stress(sigma);
    sigma -= 3.0 * kappa * alpha * (concentration() - cref) * istensor::identity();
}




// d^G/(dE dmu)
void sMechMassMP::stressChemicalTensor(istensor& M) const
{
    const double alpha = theMechMassMaterial.massExpansion;
    const double kappa = theSSMP->volumetricStiffness();
    const double R     = theMechMassMaterial.R;
    const double tref  = theMechMassMaterial.tref;

    double chic  = concentration();
    double dchidmu = chic/(R*tref);

    M = -3.0 * kappa * alpha * dchidmu * istensor::identity();
}




void sMechMassMP::tangentElasticities(itensor4& c) const
{
    theSSMP->tangentTensor(c);
}




bool sMechMassMP::testImplementation(std::ostream& os) const
{
    bool isok = true;

    // set a random update in the material
    istensor eps;
    eps.setZero();

    double temp;
    temp = randomUniform(0.7, 1.3) * theMechMassMaterial.tref;

    ivector gradMu;
    gradMu.setRandom();

    sMechMassMP* ssmp = const_cast<sMechMassMP*>(this);
    ssmp->updateCurrentState(0.0, eps, temp, gradMu);
    ssmp->commitCurrentState();

    double tn1 = muesli::randomUniform(0.1,1.0);
    eps.setRandom();
    sMechMassMP& theMP = const_cast<sMechMassMP&>(*this);
    theMP.updateCurrentState(tn1, eps, temp, gradMu);

    // programmed tangent of elasticities
    itensor4 tg;
    tangentElasticities(tg);

    // (1)  compare DEnergy with the derivative of Energy
    if (true)
    {
        // programmed stress
        istensor sigma;
        this->stress(sigma);

        // numerical differentiation stress
        istensor numSigma;
        numSigma.setZero();
        const double   inc = 1.0e-3;

        for (size_t i=0; i<3; i++)
        {
            for (size_t j=i; j<3; j++)
            {
                double original = eps(i,j);

                eps(i,j) = eps(j,i) = original + inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double Wp1 = effectiveStoredEnergy();

                eps(i,j) = eps(j,i) = original + 2.0*inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double Wp2 = effectiveStoredEnergy();

                eps(i,j) = eps(j,i) = original - inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double Wm1 = effectiveStoredEnergy();

                eps(i,j) = eps(j,i) = original - 2.0*inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double Wm2 = effectiveStoredEnergy();

                // fourth order approximation of the derivative
                double der = (-Wp2 + 8.0*Wp1 - 8.0*Wm1 + Wm2)/(12.0*inc);
                numSigma(i,j) = der;
                if (i != j) numSigma(i,j) *= 0.5;

                numSigma(j,i) = numSigma(i,j);

                eps(i,j) = eps(j,i) = original;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
            }
        }

        // relative error less than 0.01%
        istensor error = numSigma - sigma;
        isok = (error.norm()/sigma.norm() < 1e-4);

        os << "\n   1. Comparing stress with DWeff.";
        if (isok)
        {
            os << " Test passed.";
        }
        else
        {
            os << "\n      Test failed.";
            os << "\n      Relative error in DWeff computation: " <<  error.norm()/sigma.norm();
            os << "\n      Stress:\n" << sigma;
            os << "\n      Numeric stress:\n" << numSigma;
        }
    }


    // (2) compare tensor c with derivative of stress
    if (true)
    {
        // numeric C
        itensor4 nC;
        nC.setZero();

        // numerical differentiation sigma
        istensor dsigma, sigmap1, sigmap2, sigmam1, sigmam2;
        double   inc = 1.0e-3;

        for (unsigned i=0; i<3; i++)
        {
            for (unsigned j=i; j<3; j++)
            {
                double original = eps(i,j);

                eps(i,j) = eps(j,i) = original + inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                stress(sigmap1);

                eps(i,j) = eps(j,i) = original + 2.0*inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                stress(sigmap2);

                eps(i,j) = eps(j,i) = original - inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                stress(sigmam1);

                eps(i,j) = eps(j,i) = original - 2.0*inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                stress(sigmam2);

                // fourth order approximation of the derivative
                dsigma = (-sigmap2 + 8.0*sigmap1 - 8.0*sigmam1 + sigmam2)/(12.0*inc);

                if (i != j) dsigma *= 0.5;


                for (unsigned k=0; k<3; k++)
                    for (unsigned l=0; l<3; l++)
                    {
                        nC(k,l,i,j) = dsigma(k,l);
                        nC(k,l,j,i) = dsigma(k,l);
                    }

                eps(i,j) = original;
                eps(j,i) = original;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
            }
        }

        // relative error less than 0.01%
        double error = 0.0;
        double norm = 0.0;
        for (unsigned i=0; i<3; i++)
            for (unsigned j=0; j<3; j++)
                for (unsigned k=0; k<3; k++)
                    for (unsigned l=0; l<3; l++)
                    {
                        error += pow(nC(i,j,k,l)-tg(i,j,k,l),2);
                        norm  += pow(tg(i,j,k,l),2);
                    }
        error = sqrt(error);
        norm = sqrt(norm);
        isok = (error/norm < 1e-4);

        os << "\n   2. Comparing tensor C with DStress.";
        if (isok)
        {
            os << " Test passed.";
        }
        else
        {
            os << "\n      Test failed.";
            os << "\n      Relative error in DWeff computation: " <<  error/norm;
        }
    }


    // (3) compare stress and voigt stress


    // (4) compare contract tangent with cijkl
    if (true)
    {
        itensor Tvw;
        ivector v, w;
        v.setRandom();
        w.setRandom();
        contractWithTangent(v, w, Tvw);

        istensor S; S.setRandom();

        itensor nTvw; nTvw.setZero();
        for (unsigned i=0; i<3; i++)
            for (unsigned j=0; j<3; j++)
                for (unsigned k=0; k<3; k++)
                    for (unsigned l=0; l<3; l++)
                    {
                        nTvw(i,k) += tg(i,j,k,l)*v(j)*w(l);
                    }

        // relative error less than 0.01%
        itensor error = Tvw - nTvw;
        isok = (error.norm()/Tvw.norm() < 1e-4);

        os << "\n   4. Comparing contract tangent with C_ijkl v_j w_l.";
        if (isok)
        {
            os << " Test passed.";
        }
        else
        {
            os << "\n      Test failed.";
            os << "\n      Relative error: " << error.norm()/Tvw.norm();
            os << "\n      C{a,b} \n" << Tvw;
            os << "\n      C_ijkl a_j b_l:\n" << nTvw;
        }
    }


    // (5) compare volumetric and deviatoric tangent contraction

    // (6) compare tangent with volumetric+deviatoric


    return isok;
}




void sMechMassMP::updateCurrentState(const double t, const istensor& strain, const double temp, const ivector& gradMu)
{
    mu_c     = temp;
    gradMu_c = gradMu;
    theSSMP->updateCurrentState(t, strain);
}




double sMechMassMP::volumetricEnergy() const
{
    return theSSMP->volumetricEnergy();
}




double sMechMassMP::volumetricStiffness() const
{
    return theSSMP->volumetricStiffness();
}
