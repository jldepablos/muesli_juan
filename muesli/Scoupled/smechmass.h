/****************************************************************************
*
*                                 M U E S L I   v 1.8
*
*
*     Copyright 2020 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/



#pragma once
#ifndef __muesli_smechmass_h
#define __muesli_smechmass_h

#include "muesli/material.h"


namespace muesli
{
    class smallStrainMaterial;
    class smallStrainMP;
    class sMechMassMP;

    class sMechMassMaterial : public muesli::material
    {
    public:
                                sMechMassMaterial(const std::string& name,
                                                  const materialProperties& cl);
        virtual                 ~sMechMassMaterial();

        virtual bool            check() const;
        virtual sMechMassMP*    createMaterialPoint() const;
        virtual double          density() const;
        virtual double          getProperty(const propertyName p) const;
        virtual void            print(std::ostream &of=std::cout) const;
        double                  referenceTemperature() const;
        virtual void            setRandom();
        virtual bool            test(std::ostream& of=std::cout);
        virtual double          waveVelocity() const;

    private:
        smallStrainMaterial     *theSSMaterial;
        double                  cref, diffusivity, gamma, massExpansion, muref, R, tref;

        friend class            sMechMassMP;
    };


    inline double sMechMassMaterial::referenceTemperature() const {return tref;}



    class sMechMassMP : public muesli::materialPoint
    {
    public:
                                sMechMassMP(const sMechMassMaterial &m);
        virtual                 ~sMechMassMP();
        virtual void            setRandom();
        bool                    testImplementation(std::ostream& of=std::cout) const;

        // info
        double&                 chemicalPotential();
        const double&           chemicalPotential() const;
        virtual istensor        getConvergedPlasticStrain() const;
        virtual materialState   getConvergedState() const;
        virtual istensor        getCurrentPlasticStrain() const;
        virtual materialState   getCurrentState() const;
        istensor&               getCurrentStrain();
        const istensor&         getCurrentStrain() const;


        // energies
        virtual double          freeEnergy() const;
        virtual double          grandCanonicalPotential() const;
        virtual double          deviatoricEnergy() const;
        virtual double          energyDissipationInStep() const;
        virtual double          effectiveStoredEnergy() const;
        virtual double          volumetricEnergy() const;


        // gradients
        virtual double          concentration() const;
        virtual void            deviatoricStress(istensor& s) const;
        virtual ivector         massflux() const;
        virtual double          pressure() const;
        virtual void            stress(istensor& sigma) const;
        virtual void            stressChemicalTensor(istensor& M) const;
        virtual double          plasticSlip() const;


        // tangents
        virtual double          contractWithDiffusivity(const ivector &v1, const ivector &v2) const;
        virtual void            contractWithTangent(const ivector &v1,
                                                    const ivector &v2,
                                                    itensor &T) const;
        virtual void            contractWithDeviatoricTangent(const ivector &v1,
                                                              const ivector &v2,
                                                              itensor &T) const;
        virtual void            dissipationTangent(itensor4& D) const;
        virtual void            tangentElasticities(itensor4& c) const;
        virtual double          volumetricStiffness() const;


        // bookkeeping
        virtual void            commitCurrentState();
        virtual void            resetCurrentState();
        virtual void            updateCurrentState(const double t, const istensor& strain, const double mu, const ivector& gradMu);


        double                  density() const;
        const sMechMassMaterial& parentMaterial() const;


    private:
        double                  mu_n, mu_c;
        ivector                 gradMu_n, gradMu_c;
        smallStrainMP*          theSSMP;
        const sMechMassMaterial&  theMechMassMaterial;
    };

    inline double sMechMassMP::density() const  {return theMechMassMaterial.density();}
    inline const sMechMassMaterial& sMechMassMP::parentMaterial() const {return theMechMassMaterial;}
}

#endif
