/****************************************************************************
*
*                                 M U E S L I   v 1.8
*
*
*     Copyright 2020 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/



#include "conductor.h"

using namespace std;
using namespace muesli;



conductorMaterial::conductorMaterial(const std::string& name)
:
material(name)
{

}




conductorMaterial::conductorMaterial(const std::string& name,
                                       const materialProperties& cl)
:
material(name, cl)
{

}




bool conductorMaterial::check() const
{
    bool ret = true;
    return ret;
}




void conductorMaterial::setRandom()
{
    material::setRandom();
}




conductorMP::conductorMP(const conductorMaterial& mat) :
theConductor(&mat),
time_n(0.0),
time_c(0.0)
{
    gradT_n.setZero();
    gradT_c.setZero();
}




void conductorMP::commitCurrentState()
{
    time_n  = time_c;
    temp_n  = temp_c;
    gradT_n = gradT_c;
}




void conductorMP::resetCurrentState()
{
    time_c  = time_n;
    temp_c  = temp_n;
    gradT_c = gradT_n;
}




void conductorMP::setRandom()
{
    gradT_n.setRandom();
    gradT_c = gradT_n;
}




bool conductorMP::testImplementation(std::ostream& os) const
{
    bool isok = true;

    double tn1 = muesli::randomUniform(0.1,1.0);

    ivector gradT; gradT.setRandom();
    double temp = muesli::randomUniform(280.0, 1000.0);
    const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);

    // programmed heatflux and conductivity
    ivector q;  heatflux(q);
    istensor K; conductivity(K);
    ivector qp; heatfluxDerivative(qp);

    // compare heat flux with (minus) derivative of energy
    if (true)
    {
        // numerical differentiation heatflux
        ivector numFlux;
        numFlux.setZero();
        const double inc = 1.0e-3;

        for (size_t i=0; i<3; i++)
        {
            double original = gradT(i);

            gradT(i) = original + inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            double Wp1 = thermalEnergy();

            gradT(i) = original + 2.0*inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            double Wp2 = thermalEnergy();

            gradT(i) = original - inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            double Wm1 = thermalEnergy();

            gradT(i) = original - 2.0*inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            double Wm2 = thermalEnergy();


            // fourth order approximation of the derivative
            double der = - (-Wp2 + 8.0*Wp1 - 8.0*Wm1 + Wm2)/(12.0*inc);
            numFlux(i) = der;

            gradT(i) = original;
        }

        // relative error less than 0.01%
        ivector error = numFlux - q;
        isok = (error.norm()/q.norm() < 1e-4);

        os << "\n   1. Comparing heat flux with derivative of thermal energy.";

        if (!isok)
        {
            os << "\n Relative error in q computation %e. Test failed." <<  error.norm()/q.norm();
            os << "\n   Heat flux:\n" << q;
            os << "\n   Numeric heat flux:\n" << numFlux;
        }
        else
            os << " Test passed.";
    }
    

    // compare conductivity tensor with (minus) derivative of heat flux wrt temp gradient
    if (true)
    {
        istensor numK;
        numK.setZero();
        const double inc = 1.0e-3;

        for (size_t i=0; i<3; i++)
        {
            double original = gradT(i);

            gradT(i) = original + inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            ivector Qp1; heatflux(Qp1);

            gradT(i) = original + 2.0*inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            ivector Qp2; heatflux(Qp2);

            gradT(i) = original - inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            ivector Qm1; heatflux(Qm1);

            gradT(i) = original - 2.0*inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            ivector Qm2; heatflux(Qm2);

            // fourth order approximation of the derivative
            ivector der = - (-Qp2 + 8.0*Qp1 - 8.0*Qm1 + Qm2)/(12.0*inc);
            numK(0,i) = der(0);
            numK(1,i) = der(1);
            numK(2,i) = der(2);

            gradT(i) = original;
        }

        // relative error less than 0.01%
        istensor error = numK - K;
        isok = (error.norm()/q.norm() < 1e-4);

        os << "\n   2. Comparing conductivity tensor with derivative of heat flux.";

        if (!isok)
        {
            os << "\n Relative error in q computation %e. Test failed." <<  error.norm()/q.norm();
            os << "\n   Conductivity flux:\n" << K;
            os << "\n   Numeric conductivity :\n" << numK;
        }
        else
            os << " Test passed.";
    }

    // compare qprime with derivative of heat flux wrt temp
    if (true)
    {
        ivector numQ;
        numQ.setZero();
        const double inc = 1.0e-3;

        {
            double original = temp;

            temp = original + inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            ivector Qp1; heatflux(Qp1);

            temp = original + 2.0*inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            ivector Qp2; heatflux(Qp2);

            temp = original - inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            ivector Qm1; heatflux(Qm1);

            temp = original - 2.0*inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            ivector Qm2; heatflux(Qm2);

            // fourth order approximation of the derivative
            ivector der = (-Qp2 + 8.0*Qp1 - 8.0*Qm1 + Qm2)/(12.0*inc);
            numQ = der;

            temp = original;
        }

        // relative error less than 0.01%
        ivector error = numQ - qp;
        isok = (error.norm()/qp.norm() < 1e-4);

        os << "\n   3. Comparing qprime with derivative of heat flux.";

        if (!isok && qp.norm() > 1e-8)
        {
            os << "\n Relative error in q computation %e. Test failed." <<  error.norm()/q.norm();
            os << "\n   Q prime: " << qp;
            os << "\n   Numeric derivative : " << numQ;
        }
        else
            os << " Test passed.";
    }


    return isok;
}




void conductorMP::updateCurrentState(double theTime, double temp, const ivector& gradT)
{
    time_c  = theTime;
    temp_c  = temp;
    gradT_c = gradT;
}




fourierMaterial::fourierMaterial(const std::string& name,
                                   const materialProperties& cl)
:
  conductorMaterial(name, cl),
  _conductivity(0.0),
  _capacity(0.0)
{
    muesli::assignValue(cl, "conductivity", _conductivity);
    muesli::assignValue(cl, "capacity", _capacity);
}




fourierMaterial::fourierMaterial(const std::string& name, const double k, const double rho)
:
conductorMaterial(name),
_conductivity(k)
{
}




fourierMaterial::fourierMaterial(const std::string& name) :
conductorMaterial(name),
_conductivity(0.0)
{
}




bool fourierMaterial::check() const
{
    bool ret = conductorMaterial::check();

    if (_conductivity <= 0.0)
    {
        ret = false;
        std::cout << "Error in fourier material. Non-positive conductivity";
    }

    return ret;
}




conductorMP* fourierMaterial::createMaterialPoint() const
{
    fourierMP *mp = new fourierMP(*this);
    return mp;
}




double fourierMaterial::getProperty(const propertyName p) const
{
    double ret=0.0;

    if      (p == PR_CONDUCTIVITY)        ret = _conductivity;
    else if (p == PR_THERMAL_CAP)         ret = _capacity;
    else
    {
        std::cout << "\n Error in GetfourierMaterialProperty.";
    }
    return ret;
}




void fourierMaterial::print(std::ostream &of) const
{
    of  << "\n   Isotropic thermally conducting material "
        << "\n   Conductivity      : " << _conductivity
        << "\n   Density           : " << density()
        << "\n   Ref. Heat capacity: " << _capacity;
}




void fourierMaterial::setRandom()
{
    conductorMaterial::setRandom();
    _conductivity = muesli::randomUniform(1.0, 10.0);
    _capacity     = muesli::randomUniform(1.0, 10.0);
}




bool fourierMaterial::test(std::ostream& os)
{
    bool isok = true;
    setRandom();

    conductorMP* p = this->createMaterialPoint();

    isok = p->testImplementation(os);
    delete p;
    return isok;
}




fourierMP::fourierMP(const fourierMaterial& theMaterial)
:
    conductorMP(theMaterial),
    mat(&theMaterial)
{
    gradT_n.setZero();
    gradT_c.setZero();
}




void fourierMP::conductivity(istensor& K) const
{
    K = istensor::identity() * mat->_conductivity;
}




void fourierMP::contractTangent(const ivector& na, const ivector& nb, double& tg) const
{
    tg = mat->_conductivity * na.dot(nb);
}




double fourierMP::density() const
{
    return theConductor->density();
}




materialState fourierMP::getConvergedState() const
{
    materialState state;

    state.theTime = time_n;
    state.theVector.push_back(gradT_n);

    return state;
}




materialState fourierMP::getCurrentState() const
{
    materialState state;

    state.theTime = time_c;
    state.theVector.push_back(gradT_c);

    return state;
}




double fourierMP::heatCapacity() const
{
    return mat->_capacity*theConductor->density();
}




double fourierMP::heatCapacityDerivative() const
{
    return 0.0;
}




void fourierMP::heatflux(ivector &q) const
{
    q = (- mat->_conductivity) * gradT_c;
}




void fourierMP::heatfluxDerivative(ivector &qprime) const
{
    qprime.setZero();
}




void fourierMP::setRandom()
{
    conductorMP::setRandom();
}




double fourierMP::thermalEnergy() const
{
     return 0.5*mat->_conductivity * gradT_c.dot(gradT_c);
}




anisotropicConductorMaterial::anisotropicConductorMaterial(const std::string& name,
                                                           const materialProperties& cl)
:
conductorMaterial(name, cl),
_capacity(0.0)
{
    itensor t;
    muesli::assignValue(cl, "kxx", t(0,0));
    muesli::assignValue(cl, "kxy", t(0,1));
    muesli::assignValue(cl, "kxz", t(0,2));
    muesli::assignValue(cl, "kyx", t(1,0));
    muesli::assignValue(cl, "kyy", t(1,1));
    muesli::assignValue(cl, "kyz", t(1,2));
    muesli::assignValue(cl, "kzx", t(2,0));
    muesli::assignValue(cl, "kzy", t(2,1));
    muesli::assignValue(cl, "kzz", t(2,2));

    _K = istensor::symmetricPartOf(t);
    muesli::assignValue(cl, "capacity", _capacity);
}




anisotropicConductorMaterial::anisotropicConductorMaterial(const std::string& name,
                                                           double kxx, double kxy, double kxz,
                                                           double kyy, double kyz, double kzz,
                                                           double rho, double capacity)
:
conductorMaterial(name),
_capacity(capacity)
{
    _K(0,0) = kxx;
    _K(0,1) = _K(1,0) = kxy;
    _K(0,2) = _K(2,0) = kxz;
    _K(1,1) = kyy;
    _K(1,2) = _K(2,1) = kyz;
    _K(2,2) = kzz;
 }




anisotropicConductorMaterial::anisotropicConductorMaterial(const std::string& name) :
conductorMaterial(name),
_capacity(0.0)
{
    _K.setZero();
}




bool anisotropicConductorMaterial::check() const
{
    bool ret = conductorMaterial::check();

    ivector ev = _K.eigenvalues();
    if ( ev.min() <= 0.0)
    {
        ret = false;
        std::cout << "Error in anisotropicConductor material. Non-positive conductivity";
    }

    return ret;
}




conductorMP* anisotropicConductorMaterial::createMaterialPoint() const
{
    anisotropicConductorMP *mp = new anisotropicConductorMP(*this);
    return mp;
}




double anisotropicConductorMaterial::getProperty(const propertyName p) const
{
    double ret=0.0;
    return ret;
}




void anisotropicConductorMaterial::print(std::ostream &of) const
{
    of  << "\n   Anisotropic thermally conducting material "
        << "\n   Conductivity kxx   : " << _K(0,0)
        << "\n   Conductivity kxy   : " << _K(0,1)
        << "\n   Conductivity kxz   : " << _K(0,2)
        << "\n   Conductivity kyy   : " << _K(1,1)
        << "\n   Conductivity kyz   : " << _K(1,2)
        << "\n   Conductivity kzz   : " << _K(2,2)
        << "\n   Density            : " << density()
        << "\n   Ref. Heat capacity : " << _capacity;
}




void anisotropicConductorMaterial::setRandom()
{
    conductorMaterial::setRandom();
    _capacity = muesli::randomUniform(1.0, 10.0);

    _K.setRandom();
}




bool anisotropicConductorMaterial::test(std::ostream& os)
{
    bool isok = true;
    setRandom();

    conductorMP* p = this->createMaterialPoint();

    isok = p->testImplementation(os);
    delete p;

    return isok;
}




anisotropicConductorMP::anisotropicConductorMP(const anisotropicConductorMaterial& theMaterial)
:
conductorMP(theMaterial),
mat(&theMaterial)
{
    gradT_n.setZero();
    gradT_c.setZero();
}




void anisotropicConductorMP::conductivity(istensor& K) const
{
    K = mat->_K;
}




void anisotropicConductorMP::contractTangent(const ivector& na, const ivector& nb, double& tg) const
{
    tg = na.dot(mat->_K * nb);
}




double anisotropicConductorMP::density() const
{
    return theConductor->density();
}




materialState anisotropicConductorMP::getConvergedState() const
{
    materialState state;

    state.theTime = time_n;
    state.theVector.push_back(gradT_n);

    return state;
}




materialState anisotropicConductorMP::getCurrentState() const
{
    materialState state;

    state.theTime = time_c;
    state.theVector.push_back(gradT_c);

    return state;
}




double anisotropicConductorMP::heatCapacity() const
{
    return mat->_capacity*theConductor->density();
}




double anisotropicConductorMP::heatCapacityDerivative() const
{
    return 0.0;
}




void anisotropicConductorMP::heatflux(ivector &q) const
{
    q = - (mat->_K * gradT_c);
}




void anisotropicConductorMP::heatfluxDerivative(ivector &qprime) const
{
    qprime.setZero();
}




void anisotropicConductorMP::setRandom()
{
    conductorMP::setRandom();
}




double anisotropicConductorMP::thermalEnergy() const
{
    return 0.5* gradT_c.dot(mat->_K * gradT_c);
}




AMConductorMaterial::AMConductorMaterial(const std::string& name,
                                         const materialProperties& cl)
:
conductorMaterial(name, cl)
{
    muesli::assignValue(cl, "phi",phi);
    muesli::assignValue(cl, "d",  d);
    muesli::assignValue(cl, "kg", kg);
    muesli::assignValue(cl, "k0", k0);
    muesli::assignValue(cl, "k1", k1);
    muesli::assignValue(cl, "k2", k2);
    muesli::assignValue(cl, "k3", k3);
    muesli::assignValue(cl, "k4", k4);
    muesli::assignValue(cl, "k5", k5);
    muesli::assignValue(cl, "c0", c0);
    muesli::assignValue(cl, "c1", c1);
    muesli::assignValue(cl, "c2", c2);
    muesli::assignValue(cl, "c3", c3);
    muesli::assignValue(cl, "c4", c4);
    muesli::assignValue(cl, "rg", rg);
    muesli::assignValue(cl, "r0", r0);
    muesli::assignValue(cl, "r1", r1);
    muesli::assignValue(cl, "r2", r2);
    muesli::assignValue(cl, "r3", r3);
    muesli::assignValue(cl, "r4", r4);
    muesli::assignValue(cl, "r5", r5);
    muesli::assignValue(cl, "ts", tS);
    muesli::assignValue(cl, "tl", tL);
}




AMConductorMaterial::AMConductorMaterial(const std::string& name,
                                         const double _k0, const double _k1,
                                         const double rho)
:
conductorMaterial(name)
{
     k0 = _k0;
     k1 = _k1;
}




AMConductorMaterial::AMConductorMaterial(const std::string& name) :
conductorMaterial(name)
{
     k0 = 0.0;
     k1 = 0.0;
}




bool AMConductorMaterial::check() const
{
    bool ret = conductorMaterial::check();

    if (k0 <= 0.0)
    {
        ret = false;
        std::cout << "Error in nonlinear material. Non-positive conductivity at temp=0";
    }
    if (k1 <= 0.0)
    {
        ret = false;
        std::cout << "Error in nonlinear material. Non-positive conductivity";
    }

    return ret;
}




conductorMP* AMConductorMaterial::createMaterialPoint() const
{
    AMConductorMP *mp = new AMConductorMP(*this);
    return mp;
}




double AMConductorMaterial::getProperty(const propertyName p) const
{
    double ret=0.0;

    switch (p)
    {
        case PR_CONDUCTIVITY: ret = k0;      break;
        default: ret = 0.0;
    }

    return ret;
}




void AMConductorMaterial::print(std::ostream &of) const
{
    of  << "\n   AM conducting material "
        << "\n   Prosity of powder phi : " << phi
        << "\n   Av. dia. of pow. d    : " << d
        << "\n   Conductivity kg       : " << kg
        << "\n   Conductivity k0       : " << k0
        << "\n   Conductivity k1       : " << k1
        << "\n   Conductivity k2       : " << k2
        << "\n   Conductivity k3       : " << k3
        << "\n   Conductivity k4       : " << k4
        << "\n   Conductivity k5       : " << k5
        << "\n   Heat capacity c0      : " << c0
        << "\n   Heat capacity c1      : " << c1
        << "\n   Heat capacity c2      : " << c2
        << "\n   Heat capacity c3      : " << c3
        << "\n   Heat capacity c4      : " << c4
        << "\n   Density of gas rg     : " << rg
        << "\n   Density r0            : " << r0
        << "\n   Density r1            : " << r1
        << "\n   Density r2            : " << r2
        << "\n   Density r3            : " << r3
        << "\n   Density r4            : " << r4
        << "\n   Density r5            : " << r5
        << "\n   Solidus temperature   : " << tS
        << "\n   Liquidus temperature  : " << tL
        << "\n   Density               : " << density();
}




void AMConductorMaterial::setRandom()
{
    conductorMaterial::setRandom();
    phi = muesli::randomUniform(0.1, 0.5);
    d = muesli::randomUniform(20.0e-6, 80.0e-6);
    kg = muesli::randomUniform(0.01, 0.1);
    k0 = muesli::randomUniform(1.0, 10.0);
    k1 = muesli::randomUniform(100.0, 20000.0);
    k2 = muesli::randomUniform(120.0, 130.0);
    k3 = muesli::randomUniform(-0.05, -0.1);
    k4 = muesli::randomUniform(4.0, 10.0);
    k5 = muesli::randomUniform(0.01, 0.1);
    c0 = muesli::randomUniform(300.0, 900.0);
    c1 = muesli::randomUniform(0.1, 0.2);
    c2 = muesli::randomUniform(2000.0, 4000.0);
    c3 = muesli::randomUniform(1.0, 2.0);
    c4 = muesli::randomUniform(700.0, 800.0);
    rg = muesli::randomUniform(1.1, 1.8);
    r0 = muesli::randomUniform(5.0e3, 10.0e3);
    r1 = muesli::randomUniform(-0.1, -0.8);
    r2 = muesli::randomUniform(1.0e4, 2.0e4);
    r3 = muesli::randomUniform(-2.0, -9.0);
    r4 = muesli::randomUniform(4.0e3, 10.0e3);
    r5 = muesli::randomUniform(-0.1, -0.9);
    tS = muesli::randomUniform(1000.0, 1600.0);
    tL = tS + muesli::randomUniform(100.0, 200.0);
}




bool AMConductorMaterial::test(std::ostream& os)
{
    bool isok = true;
    setRandom();

    conductorMP* p = this->createMaterialPoint();

    isok = p->testImplementation(os);
    delete p;
    return isok;
}




AMConductorMP::AMConductorMP(const AMConductorMaterial& theMaterial)
:
    conductorMP(theMaterial),
    mat(&theMaterial),
    thePhase(powder)
{
}




void AMConductorMP::conductivity(istensor& K) const
{
    double phi = mat->phi;
    double ks  = mat->k0 + mat->k1 * temp_c;
    double kg  = mat->kg;
    double d   = mat->d;
    double kr  = 4.0/3.0 * 5.68e-8 * d * temp_c * temp_c * temp_c;
    double ktheta = kg * ((1.0-sqrt(1.0-phi))*(1.0+phi*kr/kg)+sqrt(1.0-phi)*((2.0/(1.0-kg/ks))*((1.0/(1.0-kg/ks))*(log(ks/kg))-1.0)+kr/kg));

    //    double ktheta = 0.0176*temp_c-3.8;
//    if(!isPowder)
//    {
    if (temp_c <= mat->tS)
    {
        ktheta = mat->k0 + mat->k1 * temp_c;
    }
    else
    {
        ktheta = mat->k2;
    }
//        ktheta = 0.015*temp_c+3.76;
//    }

    K = istensor::scaledIdentity(ktheta);
}




void AMConductorMP::contractTangent(const ivector& na, const ivector& nb, double& tg) const
{
    istensor K;
    conductivity(K);
    tg = na.dot(K*nb);
}




materialState AMConductorMP::getConvergedState() const
{
    materialState state;

    state.theTime = time_n;
    state.theVector.push_back(gradT_n);

    return state;
}




materialState AMConductorMP::getCurrentState() const
{
    materialState state;

    state.theTime = time_c;
    state.theVector.push_back(gradT_c);

    return state;
}




double AMConductorMP::density() const
{
    double phi = mat->phi;
    double rho_gas  = mat->rg;
    double rho_bulk = theConductor->density();
    double rho = (1.0 - phi) * rho_bulk + phi * rho_gas;
//    if (!isPowder)
//    {
        if (temp_c <= mat->tS)
        {
            rho = mat->r0 + mat->r1 * temp_c;
        }
        else if (temp_c <= mat->tL)
        {
            rho = mat->r2 + mat->r3 * temp_c;
        }
        else
        {
            rho = mat->r4 + mat->r5 * temp_c;
        }
//        rho = -0.199*temp_c+4454.0;
//    }
    return rho;
}




double AMConductorMP::heatCapacity() const
{
    double phi = mat->phi;
    double rho_gas  = mat->rg;
    double rho_bulk = theConductor->density();
    double rho = (1.0 - phi) * rho_bulk + phi * rho_gas;
    double c = mat->c0;
//    double c = 26650+26000*tanh((temp_c-3030.0)/100.0);
//    if (!isPowder)
//    {
        if (temp_c <= mat->tS)
        {
            rho = mat->r0 + mat->r1 * temp_c;
            c = mat->c0;
        }
        else if (temp_c <= mat->tL)
        {
            rho = mat->r2 + mat->r3 * temp_c;
            c = mat->c1;
        }
        else if (temp_c<=3030.0)
        {
            rho = mat->r4 + mat->r5 * temp_c;
            c = mat->c2;
        }
        else if (temp_c<=4000.0)
        {
            rho = mat->r4 + mat->r5 * 4000.0;
            c = 4927.0/97.0*temp_c-14858000.0/97.0;
        }
        else
        {
            rho = mat->r4 + mat->r5 * 4000.0;
            c = 50000.0;
        }
        
//        rho = -0.199*temp_c+4454.0;
//        if (temp_c <= mat->tL)
//        {
//            c = 0.18*temp_c+500.4;
//        }
//        else
//        {
//            c = 850.0;
//        }
//    }

    return c*rho;
}




double AMConductorMP::heatCapacityDerivative() const
{
    double phi = mat->phi;
    double rho_gas  = mat->rg;
    double rho_bulk = theConductor->density();
    double rho = (1.0 - phi) * rho_bulk + phi * rho_gas;
    double rhoprime = 0.0;
    double c = mat->c0;
    double cprime = 0.0;
//    double c = 26650+26000*tanh((temp_c-3030.0)/100.0);
//    double cprime = 260/cosh((temp_c-3030.0)/100);
//    if (!isPowder)
//    {
        if (temp_c <= mat->tS)
        {
            rho = mat->r0 + mat->r1 * temp_c;
            rhoprime = mat->r1;
            c = mat->c0;
            cprime = 0.0;
        }
        else if (temp_c <= mat->tL)
        {
            rho = mat->r2 + mat->r3 * temp_c;
            rhoprime = mat->r3;
            c = mat->c1;
            cprime = 0.0;
        }
        else if (temp_c<=3030.0)
        {
            rho = mat->r4 + mat->r5 * temp_c;
            rhoprime = mat->r5;
            c = mat-> c2;
            cprime = 0.0;
        }
        else if (temp_c<=4000.0)
        {
            rho = mat->r4 + mat->r5 * 4000.0;
            rhoprime = mat->r5;
            c = 4927.0/97.0*temp_c-14858000.0/97.0;
            cprime = 4927.0/97.0;
        }
        else
        {
            rho = mat->r4 + mat->r5 * 4000.0;
            rhoprime = mat->r5;
            c = 50000.0;
            cprime = 0.0;
        }
        
//        rho = -0.199*temp_c+4454.0;
//        rhoprime = -0.199;
//        if (temp_c <= mat->tL)
//        {
//            c = 0.18*temp_c+500.4;
//            cprime = 0.18;
//        }
//        else
//        {
//            c = 850.0;
//            cprime = 0.0;
//        }
//    }

    return cprime*rho+rhoprime*c;
}




void AMConductorMP::heatflux(ivector &q) const
{
    istensor K;
    conductivity(K);
    q = -(K*gradT_c);
}




void AMConductorMP::heatfluxDerivative(ivector &qprime) const
{
    double phi      = mat->phi;
    double d        = mat->d;
    double kg       = mat->kg;
    double ks       = mat->k0 + mat->k1 * temp_c;
    double ksprime  = mat->k1;
    double krprime  = (12.0 * 5.68 * pow(10.0,-8.0) * d * pow(temp_c, 2.0)) / 3.0;
    double kp       = kg * phi * (1.0-sqrt(1.0-phi)) * krprime/kg + kg * sqrt(1.0-phi) *
    (4.0*(ks/(ks-kg))*(((ks-kg)*ksprime-ks*ksprime)/((ks-kg)*(ks-kg)))*log(ks/kg) + (2.0*ksprime*kg/ks)*pow(ks/(ks-kg), 2.0) - ((2.0*ksprime*(ks-kg)-2.0*ks*ksprime)/((ks-kg)*(ks-kg))) + krprime/kg);
//    double kp = 0.0176;
//    if(!isPowder)
//    {
    if (temp_c <= mat->tS)
       {
           kp = mat->k1;
        }
        else
        {
            kp = 0.0;
        }
//        kp = 0.015;
//    }

    qprime = (-kp)*gradT_c;
}




double AMConductorMP::phase() const
{
    double phase = 0.0;

    if      (thePhase == powder) phase = 0.0;
    else if (thePhase == solid)  phase = 1.0;
    else                         phase = 2.0;

    return phase;
}




void AMConductorMP::setRandom()
{
    conductorMP::setRandom();
    thePhase = (temp_c < mat->tL) ? powder : fluid;
}




double AMConductorMP::thermalEnergy() const
{
    istensor K;
    conductivity(K);
    return 0.5 * gradT_c.dot(K*gradT_c);
}




void AMConductorMP::updateCurrentState(double theTime, double temp, const ivector& gradT)
{
    conductorMP::updateCurrentState(theTime, temp, gradT);

    if (temp_c > mat->tL)
    {
        thePhase = fluid;
    }

    else if (thePhase == fluid && temp_c < mat->tS)
    {
        thePhase = solid;
    }
}
